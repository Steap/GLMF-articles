#!/usr/bin/env python3
import getpass
import imaplib

SERVER = ''
LOGIN = ''
MAILBOXES = []


class MailFilter(object):
    def __init__(self, server, username, mailboxes):
        self.conn = imaplib.IMAP4_SSL(server)
        self.conn.login(username, getpass.getpass('Password: '))
        self.mailboxes = mailboxes

    def _search_messages(self):
        ok, uids = self.conn.uid(
                    'search', None,
                    '(UNSEEN HEADER From "Jenkins" '
                    'BODY "Jenkins has submitted this '
                    'change and it was merged")')
        assert ok == 'OK'
        # Even if there are no results, 'search' will not return an empty list.
        if uids == [b'']:
            return []
        return uids[0].split(b' ')

    def _fetch_messages(self, ids):
        ok, data = self.conn.uid(
            'fetch', b','.join(ids),
            'BODY[HEADER.FIELDS (IN-REPLY-TO)]')
        assert ok == 'OK'
        return data

    def _mark_thread_as_read(self, thread_id):
        print("Marking thread %s as read." % thread_id)
        ok, uids = self.conn.uid(
            'search', None,
            'OR HEADER In-Reply-To %s HEADER Message-Id %s' % (
                thread_id, thread_id))
        assert ok == 'OK'
        uids = uids[0].split()
        self.conn.uid('store', b','.join(uids), '+FLAGS', '\SEEN')

    def run(self):
        for mailbox in self.mailboxes:
            self.conn.select(mailbox)
            ids = self._search_messages()
            if not ids:
                continue
            data = self._fetch_messages(ids)
            for in_reply_to, _ in zip(data[0::3], data[1::3]):
                self._mark_thread_as_read(in_reply_to[1].decode()[14:-5])


if __name__ == '__main__':
    mail_filter = MailFilter(SERVER, LOGIN, MAILBOXES)
    mail_filter.run()
