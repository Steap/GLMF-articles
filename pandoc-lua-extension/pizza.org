# -*- mode: org -*-
#+TITLE: Pizza/bière façon geek
#+AUTHOR: Cyril Roelandt
#+OPTIONS: toc:nil
#+PERSONNES: 6


* Ingrédients
+ 42 bières
+ 1 téléphone

* Préparation
+ Ouvrir une bière (par personne)
+ Se saisir de son téléphone (un seul pour tout le groupe)
+ Appeler son livreur de pizza préféré
+ Commander poliment ses pizzas
+ Attendre le livreur en buvant une autre bière (par personne)
+ Ouvrir au livreur, échanger les pizzas contre de largent (les bitcoins ne seront sans doute pas acceptés)
+ Déguster les pizzas en finissant les bières
